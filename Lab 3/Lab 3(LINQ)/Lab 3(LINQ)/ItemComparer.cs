﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3_LINQ_
{
    class ItemComparer : Comparer<IItem>
    {
        public override int Compare(IItem x, IItem y)
        {
            if (x.price > y.price)
            {
                return 1;
            }
            else if (x.price < y.price)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
