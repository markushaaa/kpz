﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Lab_3_LINQ_
{
    class Toy : IItem
    {
        private ToyCategory toyCategory;

        public Toy(int price, String name, ToyCategory toyCategory)
        {
            this.price = price;
            this.name = name;
            this.toyCategory = toyCategory;
        }
       


        public override string ToString()
        {
            return String.Format("Toy: price - {0} , name - {1} , category - {2} ",price,name,toyCategory.categoryName);
        }

        public override void use()
        {
            throw new NotImplementedException();
        }
    }
}
