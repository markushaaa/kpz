﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3_LINQ_
{
    class Shop
    {

        private List<IItem> items;
        private Storage storage;

        public Shop(List<IItem> items, Storage storage)
        {
            this.items = items;
            buyHandler += buy;
            this.storage = storage;
        }

        public delegate void Buy(Object sender, IItem item);

        public Buy buyHandler;

        public void buy(Object sender, IItem item)
        {
            storage.addItem(item);
            items.Remove(item);
            Console.WriteLine("Buy item " + item.ToString());
        }

        public List<IItem> filterByType(String type)
        {
            var selectedItems = from i in items
                where i.GetType().Name == type
                orderby i.price
                select i;
            List<IItem> selected = selectedItems.ToList();
            return selected;
        }

        public void getAvaragePriceOfItems()
        {
            var e = items.GroupBy(i => i.GetType().Name);
            foreach (var g in e)
            {
                Console.WriteLine(g.Key);
                Console.WriteLine(g.Average(t => t.price));
            }
        }

        public List<IItem> getAvailableItems(int coinCount,String selectedType)
        {
            if (selectedType != "a")
            {
                return items.Where(i => (i.GetType().Name == selectedType) && i.price <= coinCount).OrderBy(i => i.price).ToList();

            }
            else
            {
               return items.Where(i => i.price <= coinCount).ToList();
            }
            
        }

        public IItem getItem(int index)
        {
            return items[index];
        }

        public override string ToString()
        {
            Console.WriteLine("Shop");
            String resString = "";
            foreach (var item in items)
            {
                resString += item.ToString() + "\n";
            }

            return resString;
        }
    }
}
