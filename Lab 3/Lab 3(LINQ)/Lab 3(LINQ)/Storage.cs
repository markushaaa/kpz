﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3_LINQ_
{
    class Storage
    {
        private List<IItem> items = new List<IItem>();

        public void addItem(IItem item)
        {
            items.Add(item);
            Console.WriteLine("Add item to storage");
        }

        public void use()
        {

        }

        public override string ToString()
        {
            Console.WriteLine("Storage");
            String resString = "";
            foreach (var item in items)
            {
                resString += item.ToString() + "\n";
            }

            return resString;
        }
    }
}
