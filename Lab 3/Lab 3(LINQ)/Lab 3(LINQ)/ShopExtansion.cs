﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3_LINQ_
{
    static class ShopExtansion
    {
        public static List<IItem> getItemsOnPositions(this Shop shop, int[] positions)
        {
            List<IItem> items = new List<IItem>();
            foreach (var position in positions)
            {
                items.Add(shop.getItem(position));
            }

            return items;
        }
    }
}
