﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3_LINQ_
{
    class ToyCategory
    {
        private int categoryId;
        public String categoryName { get; set; }

        public ToyCategory(int categoryId, String categoryName)
        {
            this.categoryId = categoryId;
            this.categoryName = categoryName;
        }
    }
}
