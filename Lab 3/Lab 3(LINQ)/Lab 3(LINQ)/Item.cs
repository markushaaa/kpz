﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3_LINQ_
{
    class Item : IItem
    {
      
        private ItemCategory itemCategory;

        public Item(int price, String name, ItemCategory itemCategory)
        {
            this.price = price;
            this.name = name;
            this.itemCategory = itemCategory;
        }


        public override string ToString()
        {
            return String.Format("Item: price - {0} , name - {1} , category - {2}",price,name,itemCategory.categoryName);
        }

        public override void use()
        {
            throw new NotImplementedException();
        }
    }
}
