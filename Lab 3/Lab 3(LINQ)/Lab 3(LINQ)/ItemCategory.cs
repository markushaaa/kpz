﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3_LINQ_
{
    class ItemCategory
    {
        private int categoryId;

        public String categoryName { get; set; }

        public ItemCategory(int categoryId, String categoryName)
        {
            this.categoryId = categoryId;
            this.categoryName = categoryName;
        }
    }
}
