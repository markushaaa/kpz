﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3_LINQ_
{
    public abstract class IItem
    {
        public int price;
        public String name;
        public abstract void use();
    }
}
