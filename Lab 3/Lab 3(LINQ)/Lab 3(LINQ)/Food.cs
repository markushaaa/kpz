﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3_LINQ_
{
    class Food : IItem
    {
        private bool isHealthy;

        public Food(int price, String name, bool isHealthy)
        {
            this.price = price;
            this.name = name;
            this.isHealthy = isHealthy;
        }

        
        public override string ToString()
        {
            return String.Format("Food: price - {0} , name - {1} , isHealthy - {2} ",price,name,isHealthy).ToString();
        }

        public override void use()
        {
            throw new NotImplementedException();
        }
    }

    
}
