﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3_LINQ_
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IItem> items = new List<IItem>()
            {
                new Food(140, "food1", true),
                new Food(170, "food2", false),
                new Item(100, "item1", new ItemCategory(1, "category1")),
                new Item(220, "item2", new ItemCategory(2, "category2")),
                new Toy(200, "Toy1", new ToyCategory(1, "category1")),
                new Toy(110, "Toy2", new ToyCategory(2, "category2")),

            };
            int coin = 500;
            String name = "Username";
            var user = new {name, coin};
            Storage storage = new Storage();
            Shop shop = new Shop(items, storage);
            Console.WriteLine("Menu");
            Console.WriteLine(user.coin + " coins left");
            Console.WriteLine("1.Get item by type(Selection by part)");
            Console.WriteLine("2.Get available items(Selection of certain information(Where))");
            Console.WriteLine("3.Buy items(List add,remove)");
            Console.WriteLine("4.Choose few items on certain position(Own extension)");
            Console.WriteLine("5.Sort items by price(IComparer)");
            Console.WriteLine("6.Dictionary");
            Console.WriteLine("7.Queue operation");
            Console.WriteLine("8.Get avarage cost of product category(group by)");
            Console.WriteLine("9.Exit");
            int code;
            while ((code = Convert.ToInt32(Console.ReadLine())) != 9)
            {
                switch (code)
                {
                    case 1:
                    {
                        Console.WriteLine("Enter type:(Toy,Item,Food)");
                        List<IItem> typeItems = shop.filterByType(Console.ReadLine());
                        foreach (var item in typeItems)
                        {
                            Console.WriteLine(item);
                        }

                        break;
                    }

                    case 2:
                    {
                        Console.WriteLine("Enter type of items(Food,Toy,Item) or 'a' for all types");
                        List<IItem> availableItems = shop.getAvailableItems(user.coin, Console.ReadLine());
                        foreach (var item in availableItems)
                        {
                            Console.WriteLine(item);
                        }

                        break;
                    }

                    case 3:
                    {
                        Console.WriteLine(shop);
                        Console.WriteLine("Choose item to buy(Number)");
                        IItem item = shop.getItem(Convert.ToInt32(Console.ReadLine()) - 1);
                        coin -= item.price;
                        user = new {user.name, coin};
                        shop.buyHandler.Invoke(shop, item);
                        Console.WriteLine("\n\nUpdated shop");
                        Console.WriteLine(shop);
                        Console.WriteLine("\n\nUpdated storage");
                        Console.WriteLine(storage);
                        break;
                    }
                    case 4:
                    {
                        Console.WriteLine("\n\n");
                        Console.WriteLine(shop);
                        Console.WriteLine("\n\nEnter positions:(0 to end)");
                        List<int> positions = new List<int>();
                        int position;
                        while ((position = Convert.ToInt32(Console.ReadLine())) != 0)
                        {
                            Console.WriteLine("Next position:");
                            positions.Add(position - 1);
                        }

                        foreach (var item in shop.getItemsOnPositions(positions.ToArray()))
                        {
                            Console.WriteLine(item);   
                        }

                        break;
                    }
                    case 5:
                    {
                        Console.WriteLine("Unsorted items");
                        List<IItem> sItems = new List<IItem>()
                        {
                            new Food(140, "food1", true),
                            new Food(170, "food2", false),
                            new Item(100, "item1", new ItemCategory(1, "category1")),
                            new Item(220, "item2", new ItemCategory(2, "category2")),
                            new Toy(200, "Toy1", new ToyCategory(1, "category1")),
                            new Toy(110, "Toy2", new ToyCategory(2, "category2")),

                        };
                        foreach (var item in sItems)
                        {
                            Console.WriteLine(item);    
                        }
                        sItems.Sort(new ItemComparer());
                        Console.WriteLine("\n\nSorted items");
                        foreach (var item in sItems)
                        {
                            Console.WriteLine(item);
                        }

                        break;
                    }

                    case 6:
                    {
                        Console.WriteLine("ToyCategory - ToyList");
                        List<Toy> toyCategory1 = new List<Toy>()
                        {
                            new Toy(200, "Toy1", new ToyCategory(1, "category1")),
                            new Toy(120, "Toy2", new ToyCategory(1, "category1")),
                            new Toy(150, "Toy4", new ToyCategory(1, "category1")),
                            new Toy(165, "Toy6", new ToyCategory(1, "category1"))
                        };
                        List<Toy> toyCategory2 = new List<Toy>()
                        {
                            new Toy(140, "Toy3", new ToyCategory(2, "category2")),
                            new Toy(125, "Toy5", new ToyCategory(2, "category2")),
                            new Toy(122, "Toy7", new ToyCategory(2, "category2"))
                        };

                        List<Toy> toyCategory3 = new List<Toy>()
                        {
                            new Toy(130, "Toy3", new ToyCategory(3, "category3")),
                            new Toy(175, "Toy5", new ToyCategory(3, "category3")),
                            new Toy(112, "Toy7", new ToyCategory(3, "category3"))
                        };

                        Dictionary<int, List<Toy>> toys = new Dictionary<int, List<Toy>>();
                        toys.Add(1, toyCategory1);
                        toys.Add(2, toyCategory2);
                        toys.Add(3, toyCategory3);

                        foreach (var toy in toys)
                        {
                            Console.WriteLine(toy.Key);
                            foreach (var toyItem in toy.Value)
                            {
                                Console.WriteLine(toyItem);
                            }
                        }

                        break;
                    }
                    case 7:
                    {
                        Queue<IItem> itemsQueue = new Queue<IItem>();
                        itemsQueue.Enqueue(new Food(140, "food1", true));
                        itemsQueue.Enqueue(new Item(100, "item1", new ItemCategory(1, "category1")));
                        itemsQueue.Enqueue(new Toy(200, "Toy1", new ToyCategory(1, "category1")));
                        itemsQueue.Enqueue(new Toy(110, "Toy2", new ToyCategory(1, "category2")));
                        while (itemsQueue.Count != 0)
                        {
                            Console.WriteLine(itemsQueue.Dequeue());
                        }

                        Console.WriteLine("Queue is empty");
                        break;
                      
                    }

                    case 8:
                    {
                        shop.getAvaragePriceOfItems();
                        break;
                    }

                }
                Console.WriteLine("\n\n\nMenu");
                Console.WriteLine(user.coin + " coins left");
                Console.WriteLine("1.Get item by type(Selection by part)");
                Console.WriteLine("2.Get available items(Selection of certain information(Where))");
                Console.WriteLine("3.Buy items(List add,remove)");
                Console.WriteLine("4.Choose few items on certain position(Own extension)");
                Console.WriteLine("5.Sort items by price(IComparer)");
                Console.WriteLine("6.Dictionary");
                Console.WriteLine("7.Queue operation");
                Console.WriteLine("8.Get avarage cost of product category(group by)");

            }
        }

    }
}
