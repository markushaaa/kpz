﻿using System;
using System.Collections.Generic;
using System.Linq;
using Robot.Common;
using  Popyk.Marko.RobotChallange;
using static Popyk.Marko.RobotChallange.DistanceHelper;
namespace Popyk.Marko.RobotChallange
{
    public class PopykAlgorithm : IRobotAlgorithm
    {
        private static int roundNumber = 0;

        public static int getRoundNumber()
        {
            return roundNumber;
        }

        public PopykAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            roundNumber++;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            Position robotPosition = movingRobot.Position;
            Position stationPosition = DistanceHelper.FindNearestFreeStation(movingRobot, map, robots);
           
            /*int energyForNewRobot = DistanceHelper.calculateNewRobotEnergy(movingRobot, map, robots);
            if (energyForNewRobot > 0)
            {
                return new CreateNewRobotCommand()
                {
                    NewRobotEnergy =  energyForNewRobot
                };
            }*/
            int myRobots = robots.Count(r => r.OwnerName == "Popyk Marko");
            EnergyStation energyStation = null;
            if (movingRobot.Energy > 200 && myRobots < 100)
            {
                return new CreateNewRobotCommand()
                {
                    NewRobotEnergy = 100
                };
            }
           
            EnergyStation robotStation = null;
            bool IsRobotInsideAnyStationRadius =
                DistanceHelper.IsRobotInsideAnyStationRadius(map, movingRobot.Position, out robotStation);
            if (robotStation != null)
            {
                int RobotsInsideStationRadius = DistanceHelper.RobotsInsideStationRadius(robotStation.Position, robots);
                if (IsRobotInsideAnyStationRadius && (DistanceHelper.energyWillCollect(map, movingRobot) >= 40 || getRoundNumber() < 2))
                {
                    return new CollectEnergyCommand();
                }
            }
            Position bestPositionNearStation = DistanceHelper.chooseBestCellToStay(movingRobot, stationPosition, robots);


           Position nextPosition = DistanceHelper.getNextCell(movingRobot, bestPositionNearStation,robots);

            return new MoveCommand()
                {
                    NewPosition = nextPosition
                };

        }

        public string Author
        {
            get
            {
                return "Popyk Marko";
            }
        }
    }
}
