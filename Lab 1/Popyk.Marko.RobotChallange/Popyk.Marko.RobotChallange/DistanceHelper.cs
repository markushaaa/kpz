﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using Robot.Common;
namespace Popyk.Marko.RobotChallange
{
   public class DistanceHelper
    {
        public static int FindDistance(Position a, Position b)
        {
            return (int) (Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

        public static Position FindMiddleOfSegment(Position a, Position b)
        {
            return new Position((a.X + b.X) / 2, (a.Y + b.Y) / 2);
        }


        public static Position getNextCell(Robot.Common.Robot movingRobot, Position stationPosition,
            IList<Robot.Common.Robot> robots)
        {
            int needMove = needMoveToGetToNearestStation(movingRobot, stationPosition);
            Position lastPosition = stationPosition;
            for (int i = 1; i < needMove; i++)
            {
                lastPosition = FindMiddleOfSegment(movingRobot.Position, lastPosition);
            }

            if (!IsCellFree(lastPosition, movingRobot, robots))
            {
                return new Position(lastPosition.X + 1, lastPosition.Y);
            }

            return lastPosition;
        }


        public static int needMoveToGetToNearestStation(Robot.Common.Robot movingRobot, Position stationPosition)
        {
            int move = 1;
            Position currentPosition = stationPosition;
            while (move < movingRobot.Energy)
            {
                if (movingRobot.Energy >= DistanceHelper.FindDistance(movingRobot.Position, currentPosition) * move)
                {
                        return move;
                }

                move++;
                currentPosition = FindMiddleOfSegment(movingRobot.Position, currentPosition);
            }

            return 0;
        }

        public static Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            double prevProfit = 0;
            EnergyStation bestStation = null;
            foreach (var station in map.Stations)
            {
                if (IsRobotInsideCertainStationRadius(station.Position, movingRobot.Position))
                {
                    continue;
                }

                int needMoveToGet = needMoveToGetToNearestStation(movingRobot, station.Position);
                Position bestPosition = chooseBestCellToStay(movingRobot, station.Position, robots);

                int energyRobotWillLostToGeToStation = needMoveToGet *
                                                       FindDistance(movingRobot.Position,
                                                           getNextCell(movingRobot, bestPosition, robots));

                if (energyRobotWillLostToGeToStation > movingRobot.Energy)
                {
                    continue;
                }

                int robotsInsideStationRadius = RobotsInsideStationRadius(station.Position, robots);
                int energyRobotWillGet = 0;
                
                if (robotsInsideStationRadius > 1)
                {
                    int energyStationWillRemain = station.Energy - needMoveToGet * (robotsInsideStationRadius * 40) +
                                                  needMoveToGet * 75;


                    energyRobotWillGet = 40 * energyStationWillRemain / robotsInsideStationRadius;
                }
                else
                {
                    energyRobotWillGet = (50 - (needMoveToGet + PopykAlgorithm.getRoundNumber())) * 40;
                }

                double profit = (double)energyRobotWillGet / (needMoveToGet*FindDistance(movingRobot.Position,station.Position));

                if (profit < 0)
                {
                    continue;
                }

                if (profit > prevProfit)
                {
                    bestStation = station;
                    prevProfit = profit;
                }
            }

            return bestStation == null ? null : bestStation.Position;
        }



        public static bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }

            return true;
        }

        public static bool IsRobotInsideAnyStationRadius(Map map, Position robotPosition, out EnergyStation eStation)
        {
            foreach (var station in map.Stations)
            {
                if ((Math.Abs(station.Position.X - robotPosition.X) <= 2) &&
                    (Math.Abs(station.Position.Y - robotPosition.Y) <= 2))
                {
                    eStation = station;
                    return true;
                }
            }

            eStation = null;
            return false;
        }


        public static bool IsRobotInsideCertainStationRadius(Position stationPosition, Position robotPosition)
        {
            if ((Math.Abs(stationPosition.X - robotPosition.X) <= 2) &&
                (Math.Abs(stationPosition.Y - robotPosition.Y) <= 2))
            {
                return true;
            }

            return false;
        }


        public static int RobotsInsideStationRadius(Position stationPosition,
            IList<Robot.Common.Robot> robots)
        {
            int robotsInsideStationRadius = 0;
            foreach (Robot.Common.Robot robot in robots)
            {
                if (IsRobotInsideCertainStationRadius(stationPosition, robot.Position))
                {
                    robotsInsideStationRadius++;
                }
            }

            return robotsInsideStationRadius;
        }


        public static bool IsEnoughEnergyToMove(Position positionToMove, Robot.Common.Robot movingRobot)
        {
            return DistanceHelper.FindDistance(movingRobot.Position, positionToMove) <= movingRobot.Energy;
        }

        public static Position ChooseCellToStayIfStationIsNotFree(Position stationPosition,
            Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            for (int i = stationPosition.X - 2; i <= stationPosition.X + 2; i++)
            {
                for (int j = stationPosition.Y - 2; j < stationPosition.Y + 2; j++)
                {
                    if (IsCellFree(new Position(i, j), movingRobot, robots))
                    {
                        return new Position(i, j);
                    }
                }
            }

            return stationPosition;
        }

        public static Position chooseBestCellToStay(Robot.Common.Robot movingRobot, Position stationPosition,
            IList<Robot.Common.Robot> robots)
        {

            //if robot down-left
            if (movingRobot.Position.X < stationPosition.X && movingRobot.Position.Y < stationPosition.Y)
            {
                for (int radius = 0; radius <= 4; radius++)
                {
                    Position startPosition = new Position(stationPosition.X - 2, stationPosition.Y - 2 + radius);
                    for (int x = 0; x <= radius; x++)
                    {
                        Position checkPosition = new Position(startPosition.X + x, startPosition.Y);
                        if (IsCellFree(checkPosition, movingRobot, robots))
                        {
                            return checkPosition;
                        }
                    }

                    for (int y = radius; y >= 0; y--)
                    {
                        Position checkPosition = new Position(startPosition.X + radius, startPosition.Y - y);
                        if (IsCellFree(checkPosition, movingRobot, robots))
                        {
                            return checkPosition;
                        }
                    }
                }
            }

            //if robot top-left
            else if (movingRobot.Position.X < stationPosition.X && movingRobot.Position.Y > stationPosition.Y)
            {
                for (int radius = 0; radius <= 4; radius++)
                {
                    Position startPosition = new Position(stationPosition.X - 2, stationPosition.Y + 2 - radius);
                    for (int x = 0; x <= radius; x++)
                    {
                        Position checkPosition = new Position(startPosition.X + x, startPosition.Y);
                        if (IsCellFree(checkPosition, movingRobot, robots))
                        {
                            return checkPosition;
                        }
                    }

                    for (int y = 0; y <= radius; y++)
                    {
                        Position checkPosition = new Position(startPosition.X + radius, startPosition.Y - y);
                        if (IsCellFree(checkPosition, movingRobot, robots))
                        {
                            return checkPosition;
                        }
                    }
                }

            }
            // if robot top-right

            else if (movingRobot.Position.X > stationPosition.X && movingRobot.Position.Y > stationPosition.Y)
            {
                for (int radius = 0; radius <= 4; radius++)
                {
                    Position startPosition = new Position(stationPosition.X + 2, stationPosition.Y + 2 - radius);
                    for (int x = 0; x <= 4; x++)
                    {
                        Position checkPosition = new Position(startPosition.X - x, startPosition.Y);
                        if (IsCellFree(checkPosition, movingRobot, robots))
                        {
                            return checkPosition;
                        }
                    }

                    for (int y = 0; y <= 4; y++)
                    {
                        Position checkPosition = new Position(startPosition.X + radius, startPosition.Y + y);
                        if (IsCellFree(checkPosition, movingRobot, robots))
                        {
                            return checkPosition;
                        }
                    }
                }

            }
            // if robot down-right

            else if (movingRobot.Position.X > stationPosition.X && movingRobot.Position.Y < stationPosition.Y)
            {
                for (int radius = 0; radius <= 4; radius++)
                {
                    Position startPosition = new Position(stationPosition.X + 2, stationPosition.Y - 2 + radius);
                    for (int x = 0; x <= 4; x++)
                    {
                        Position checkPosition = new Position(startPosition.X - x, startPosition.Y);
                        if (IsCellFree(checkPosition, movingRobot, robots))
                        {
                            return checkPosition;
                        }
                    }

                    for (int y = 0; y <= 4; y++)
                    {
                        Position checkPosition = new Position(startPosition.X + radius, startPosition.Y - y);
                        if (IsCellFree(checkPosition, movingRobot, robots))
                        {
                            return checkPosition;
                        }
                    }
                }

            }

            // left 

            else if (movingRobot.Position.X < stationPosition.X && movingRobot.Position.Y == stationPosition.Y)
            {
                for (int x = stationPosition.X -2; x <= stationPosition.X + 2; x++)
                {
                    for (int y = stationPosition.Y - 2; y < stationPosition.Y + 2; y++)
                    {
                        if (IsCellFree(new Position(x, y), movingRobot, robots))
                        {
                            return new Position(x, y);
                        }
                    }
                }
            }

            //right 
            else if (movingRobot.Position.X > stationPosition.X && movingRobot.Position.Y == stationPosition.Y)
            {
                for (int x = stationPosition.X + 2; x >= stationPosition.X - 2; x--)
                {
                    for (int y = stationPosition.Y - 2; y < stationPosition.Y + 2; y++)
                    {
                        if (IsCellFree(new Position(x, y), movingRobot, robots))
                        {
                            return new Position(x, y);
                        }
                    }
                }
            }

            // top 

            else if (movingRobot.Position.X == stationPosition.X && movingRobot.Position.Y > stationPosition.Y)
            {
                Position startPosition = new Position(stationPosition.X, stationPosition.Y + 2);
                for (int y = startPosition.Y; y >= startPosition.Y - 2; y--)
                {
                    for (int x = startPosition.X - 2; x < startPosition.X + 2; x++)
                    {
                        if (IsCellFree(new Position(x, y), movingRobot, robots))
                        {
                            return new Position(x, y);
                        }
                    }
                }
            }


            // down


            else if (movingRobot.Position.X == stationPosition.X && movingRobot.Position.Y < movingRobot.Position.Y)
            {
                Position startPosition = new Position(stationPosition.X, stationPosition.Y - 2);
                for (int y = startPosition.Y; y <= startPosition.Y + 2; y++)
                {
                    for (int x = startPosition.X - 2; x < startPosition.X + 2; x++)
                    {
                        if (IsCellFree(new Position(x, y), movingRobot, robots))
                        {
                            return new Position(x, y);
                        }
                    }
                }
            }



            return stationPosition;
        }



        public static int calculateNewRobotEnergy(Robot.Common.Robot creatingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            Position nearestStation = null;
            EnergyStation robotStation = null;
            if (creatingRobot.Energy < 100)
            {
                return -1;
            }


            bool isRobotInsideAnyStationRadius =
                DistanceHelper.IsRobotInsideAnyStationRadius(map, creatingRobot.Position, out robotStation);
            if (isRobotInsideAnyStationRadius)
            {
                int robotsInsideStationRadius = RobotsInsideStationRadius(robotStation.Position, robots);
                int minusEnergyForRound = 40 * robotsInsideStationRadius;
                int minusEnergyForRoundAfterCreate = (robotsInsideStationRadius + 1) * 40;
                int willRemainEnergyAfterCreate = robotStation.Energy - minusEnergyForRound;
                int countOfRoundsWhileEnergyInStationMoreThanZero = 0;
                while (willRemainEnergyAfterCreate > 0)
                {
                    willRemainEnergyAfterCreate -= minusEnergyForRoundAfterCreate * 40;
                    if (willRemainEnergyAfterCreate < 0)
                    {
                        break;
                    }
                    willRemainEnergyAfterCreate += 75;
                    countOfRoundsWhileEnergyInStationMoreThanZero++;
                }

                int energyNewRobotWillGet = countOfRoundsWhileEnergyInStationMoreThanZero * 40;
                nearestStation = DistanceHelper.FindNearestFreeStation(creatingRobot, map, robots);
                if (energyNewRobotWillGet > 100)
                {
                    return 8;
                }
            }

            return -1;
        }
        public static int energyWillCollect(Map map, Robot.Common.Robot movingRobot)
        {
            int energy = 0;
            foreach(EnergyStation station in map.Stations)
            {
                if(IsRobotInsideCertainStationRadius(station.Position,movingRobot.Position))
                {
                    energy += station.Energy;
                }
            }
            return energy;
        }
    }
}

