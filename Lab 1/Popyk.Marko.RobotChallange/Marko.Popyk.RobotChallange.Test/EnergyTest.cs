﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Popyk.Marko.RobotChallange;
using Robot.Common;
namespace Marko.Popyk.RobotChallange.Test
{
    [TestClass]
    public class EnergyTest
    {
        [TestMethod]
        public void WillCollectEnergyTest()
        {
            Map map = new Map();
            Robot.Common.Robot robot = new Robot.Common.Robot() { Position = new Position(21, 21) };
            EnergyStation station1 = new EnergyStation() { Energy = 40, Position = new Position(20, 20) };
            EnergyStation station2 = new EnergyStation() { Energy = 40, Position = new Position(22, 22) };
            map.Stations.Add(station1);
            map.Stations.Add(station2);
            int expectedValue = 80;
            Assert.AreEqual(expectedValue,DistanceHelper.energyWillCollect(map,robot));
        }

        [TestMethod]
        public void IsEnoughEnergyToMoveTest()
        {
            Robot.Common.Robot robot = new Robot.Common.Robot() { Position = new Position(0, 0), Energy = 50 };
            Position positionToMove = new Position(10, 15);
            Assert.IsFalse(DistanceHelper.IsEnoughEnergyToMove(positionToMove, robot));
        }
    }
}
