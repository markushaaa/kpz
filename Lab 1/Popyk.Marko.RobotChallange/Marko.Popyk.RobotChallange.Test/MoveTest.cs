using Microsoft.VisualStudio.TestTools.UnitTesting;
using Popyk.Marko.RobotChallange;
using Robot.Common;
using System.Collections.Generic;

namespace Marko.Popyk.RobotChallange.Test
{
    [TestClass]
    public class MoveTest
    {
        [TestMethod]
        public void needMoveToGetToNearestStationTest()
        {
            Map map = new Map();
            Position robotPosition = new Position(0, 0);
            Position stationPosition = new Position(14, 14);
            Robot.Common.Robot robot = new Robot.Common.Robot() { Energy = 200, Position = robotPosition };
            int expectedValue = 2;
            Assert.AreEqual(expectedValue, DistanceHelper.needMoveToGetToNearestStation(robot, stationPosition));
        }

       
        [TestMethod]
        public void getNextCellTest()
        {
            Map map = new Map();
            Position robotPosition = new Position(0, 0);
            Position stationPosition = new Position(14, 14);
            Robot.Common.Robot robot = new Robot.Common.Robot() { Energy = 200, Position = robotPosition };
            Position expectedPosition = DistanceHelper.FindMiddleOfSegment(robotPosition, stationPosition);
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(robot);
            Assert.AreEqual(expectedPosition,DistanceHelper.getNextCell(robot,stationPosition,robots));
        }

        [TestMethod]

        public void chooseBestCellToStayTest()
        {
            Map map = new Map();
            Position robotPosition = new Position(50, 50);
            Position leftTopStation = new Position(45, 55);
            Position rightTopStation = new Position(55, 55);
            Position leftDownStation = new Position(45, 45);
            Position rightDownStation = new Position(55, 45);
            
            Robot.Common.Robot robot = new Robot.Common.Robot() { Energy = 5000, Position = robotPosition };
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(robot);
            Position[] expectedPositions = new Position[4] {
                new Position(47,53),
                new Position(53, 53),
                new Position(47, 47),
                new Position(53, 47) 
            };
            Position[] actualPositions = new Position[4]
            {
                DistanceHelper.chooseBestCellToStay(robot,leftTopStation,robots),
                DistanceHelper.chooseBestCellToStay(robot,rightTopStation,robots),
                DistanceHelper.chooseBestCellToStay(robot,leftDownStation,robots),
                DistanceHelper.chooseBestCellToStay(robot,rightDownStation,robots)
            };

            CollectionAssert.AreEqual(expectedPositions, actualPositions);
        }
    }
}
