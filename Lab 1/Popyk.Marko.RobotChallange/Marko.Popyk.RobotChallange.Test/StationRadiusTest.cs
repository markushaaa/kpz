﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Popyk.Marko.RobotChallange;
using Robot.Common;

namespace Marko.Popyk.RobotChallange.Test
{
    [TestClass]
    public class StationRadiusTest
    {
        [TestMethod]
        public void howManyRobotsInsideStationRadiusTest()
        {
            Position stationPosition = new Position(20, 20);
            Position robot1Position = new Position(20, 20);
            Position robot2Position = new Position(21, 21);
            Position robot3Position = new Position(18, 18);
            Robot.Common.Robot robot1 = new Robot.Common.Robot() { Energy = 200, Position = robot1Position };
            Robot.Common.Robot robot2 = new Robot.Common.Robot() { Energy = 200, Position = robot2Position };
            Robot.Common.Robot robot3 = new Robot.Common.Robot() { Energy = 200, Position = robot3Position };
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(robot1);
            robots.Add(robot2);
            robots.Add(robot3);
            int expected = 3;
            Assert.AreEqual(expected, DistanceHelper.RobotsInsideStationRadius(stationPosition, robots));
        }

        [TestMethod]
        public void isRobotInsideAnyStationRadiusTest()
        {
            Map map = new Map();
            EnergyStation station = new EnergyStation() { Position = new Position(20, 20) };
            map.Stations.Add(station);
            Position robotPostion = new Position(19, 21);
            Assert.IsTrue(DistanceHelper.IsRobotInsideAnyStationRadius(map, robotPostion, out station));
        }
    }
}
