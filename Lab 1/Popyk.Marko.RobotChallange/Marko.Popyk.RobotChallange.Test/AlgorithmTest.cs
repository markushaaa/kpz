﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Popyk.Marko.RobotChallange;
using Robot.Common;
namespace Marko.Popyk.RobotChallange.Test
{
    [TestClass]
    public class AlgorithmTest
    {
        [TestMethod]
        public void CollectTest()
        {
            PopykAlgorithm algorithm = new PopykAlgorithm();
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            Map map = new Map();
            Robot.Common.Robot robot = new Robot.Common.Robot() { Position = new Position(20, 20), Energy = 20 };
            EnergyStation energyStation = new EnergyStation() { Energy = 100, Position = new Position(21, 21) };
            map.Stations.Add(energyStation);
            robots.Add(robot);
            RobotCommand command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is CollectEnergyCommand);
        }

        [TestMethod]
        public void CreateTest()
        {
            PopykAlgorithm algorithm = new PopykAlgorithm();
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            Map map = new Map();
            Robot.Common.Robot robot = new Robot.Common.Robot() { Position = new Position(20, 20), Energy = 250};
            EnergyStation energyStation = new EnergyStation() { Energy = 100, Position = new Position(21, 21) };
            map.Stations.Add(energyStation);
            robots.Add(robot);
            RobotCommand command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is CreateNewRobotCommand);
        }


        [TestMethod]
        public void MoveTest()
        {
            PopykAlgorithm algorithm = new PopykAlgorithm();
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            Map map = new Map();
            Robot.Common.Robot robot = new Robot.Common.Robot() { Position = new Position(25, 25) };
            EnergyStation energyStation = new EnergyStation() { Energy = 100, Position = new Position(21, 21) };
            map.Stations.Add(energyStation);
            robots.Add(robot);
            RobotCommand command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is MoveCommand);
        }
    }
}
